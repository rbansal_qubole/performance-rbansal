import logging
import sys

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


class Logger:

    log = logging.getLogger()

    def __init__(self, stream=sys.stdout, level=logging.DEBUG,
                 format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'):
        ch = logging.StreamHandler(stream)
        ch.setLevel(level)
        formatter = logging.Formatter(format)
        ch.setFormatter(formatter)
        self.log.addHandler(ch)
