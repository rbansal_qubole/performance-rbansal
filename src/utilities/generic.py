import subprocess


def execute_shell_command(command):
    process = subprocess.Popen(
        command,
        shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT)
    stdout, stderr = process.communicate()
    return_code = process.returncode
    return stdout, stderr, return_code


def execute_ssh_command(hostname, user, key, command):
    process = subprocess.Popen(["ssh", "-o StrictHostKeyChecking=no", "-i", key, user + "@" + hostname, command],
                               shell=False,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    return_code = process.returncode
    return stdout, stderr, return_code


def to_seconds(time_str):
    time_in_sec = 0
    if time_str.endswith('ms'):
        time_in_sec = float(time_str.replace('ms', '')) / 1000
    elif time_str.endswith('s'):
        time_in_sec = float(time_str.replace('s', ''))
    elif time_str.endswith('m'):
        time_in_sec = float(time_str.replace('m', '')) * 60
    elif time_str.endswith('h'):
        time_in_sec = float(time_str.replace('h', '')) * 3600

    return time_in_sec
