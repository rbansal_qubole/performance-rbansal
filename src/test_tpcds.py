from src.utilities.logger import Logger
from src.builder.presto_emr_builder import PrestoEMRBuilder
import os


def test_performance(results):
    # if platform is EMR, use PrestoEMRBuilder
    builder = PrestoEMRBuilder()

    # create cluster
    if os.getenv('create_cluster') == 'true':
        Logger.log.info("Creating the cluster......")
        jobflow_id, cluster_arn, emr_master_dns = builder.create_cluster(os.getenv('cluster_name'),
                                                                         os.getenv('log_uri'),
                                                                         os.getenv('release_label'),
                                                                         os.getenv('instance_type'),
                                                                         os.getenv('instance_count'),
                                                                         os.getenv('ec2_key_name'),
                                                                         os.getenv('aws_subnet_id'),
                                                                         os.getenv('aws_security_group'),
                                                                         os.getenv('applications'),
                                                                         os.getenv('tags'), os.getenv('configurations'))
        Logger.log.info("JobFlow ID: %s, DNS Name: %s" % (jobflow_id, emr_master_dns))
        if emr_master_dns is not 'null':
            os.environ["emr_master_dns"] = emr_master_dns

    # show session
    #Logger.log.info("JobFlowID: %s" %(jobflow_id))
    #Logger.log.info("ClusterArn: %s" %(cluster_arn))
    if os.getenv('emr_master_dns') == '':
        assert "MasterPublicDnsName is empty."
    Logger.log.info("MasterPublicDnsName: %s" % os.getenv('emr_master_dns'))
    Logger.log.info("Checking if cluster is up......")
    cluster_up = builder.show_session(os.getenv('emr_master_dns'))
    assert cluster_up, "Show session failed -> cluster is not up."

    Logger.log.info("Show session successful -> cluster is up.")

    # setup sql queries using hive command
    Logger.log.info("skip setup queries : %s" % os.getenv('skip_setup_queries'))
    setup_completed = True
    if os.getenv('skip_setup_queries') == 'false':
        setup_completed = builder.run_setup_queries(jobflow_id, os.getenv('setup_script_location'))
        Logger.log.info("step - Setup queries completed: %s" % setup_completed)

    if setup_completed:
        if os.getenv('run_tests') == 'true':

            # TPCDS queries
            query_location = os.getenv('workload_loc')
            Logger.log.info("list_of_queries_to_run: %s" % os.getenv('list_of_queries_to_run'))

            if os.getenv('list_of_queries_to_run') == 'all':
                query_list = os.listdir(query_location)
                Logger.log.info("Running all the queries from the workload location %s ......" % query_list)
            else:
                query_list = os.getenv('list_of_queries_to_run').split(',')
                Logger.log.info(
                    "Running all the queries from the list_of_queries_to_run provided %s ......" % query_list)

            for query_filename in sorted(query_list):
                query_path = query_location + query_filename

                # reading the sql file and substituting the variables
                with open(query_path, 'r') as f:
                    content = f.read()
                content = content.replace("$db_name", os.getenv('db_name'))
                with open(query_path, 'w') as f:
                    f.write(content)

                # Multiple iterations of each query execution
                for x in range(int(os.getenv('query_iteration'))):
                    query_passed, query_info, query_stats, elapsed_time, stdout, error_info = \
                        builder.run_query(query_filename, query_path, os.getenv('emr_master_dns'))

                    # dumping the results in files after each query
                    if query_passed:
                        Logger.log.info("Query(%s) Execution successful in %s seconds." % (query_filename, elapsed_time))
                        results.update_pass_results(query_filename, elapsed_time, query_stats)
                        results.update_output(query_filename, stdout)

                    else:
                        Logger.log.info("Query(%s) Execution failed." % query_filename)
                        results.update_failed_queries(query_filename + "_" + str(x), error_info)

                # write query_engine_runtimes in a file
                results.dump_results()
        else:
            Logger.log.info("Setup queries failed, TPCDS queries will not be executed.")

        # Teardown setup
        # yield setup_queries

        if os.getenv('terminate_cluster') == 'true':
            Logger.log.info("Terminating the cluster......")
            builder.terminate_cluster(jobflow_id)
