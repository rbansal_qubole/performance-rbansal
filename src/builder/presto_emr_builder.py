import time
import json
import os
import boto3
import requests
from src.builder.builder import Builder
from src.utilities.logger import Logger
from src.utilities.generic import execute_shell_command
from src.utilities.generic import to_seconds


class PrestoEMRBuilder(Builder):

    def __init__(self):
        self.client = boto3.client('emr', region_name='us-east-1')

    def create_cluster(self, cluster_name, log_uri, release_label, instance_type, instance_count, ec2_key_name,
                       aws_subnet_id, aws_security_group, applications, tags, configurations):
        apps_list = applications.split(',')
        apps = list()
        for app in apps_list:
            apps.append(dict(Name=app))

        tags_list = tags.split(',')
        tags_param = list()
        for tag in tags_list:
            key_values = tag.split('=')
            tags_param.append(dict(Key=key_values[0], Value=key_values[1]))

        Logger.log.info("configurations: %s" % configurations)

        instance_grps = list()
        instance_grps.append(dict(InstanceRole='MASTER', InstanceType=instance_type, InstanceCount=1,
                                  Configurations=json.loads(str(configurations))))
        instance_grps.append(dict(InstanceRole='CORE', InstanceType=instance_type, InstanceCount=int(instance_count)-1,
                                  Configurations=json.loads(str(configurations))))

        cluster_id = self.client.run_job_flow(
            Name=cluster_name,
            LogUri=log_uri,
            ReleaseLabel=release_label,
            Instances={
                'InstanceGroups': instance_grps,
                'KeepJobFlowAliveWhenNoSteps': True,
                'TerminationProtected': False,
                'Ec2KeyName': ec2_key_name,
                'Ec2SubnetId': aws_subnet_id,
                'EmrManagedMasterSecurityGroup': aws_security_group,
                'EmrManagedSlaveSecurityGroup': aws_security_group,
            },
            Steps=[
                {
                    'Name': 'Setup hadoop debugging',
                    'ActionOnFailure': 'CONTINUE',
                    'HadoopJarStep': {
                        'Jar': 'command-runner.jar',
                        'Args': ['state-pusher-script']
                    }
                }
            ],
            Applications=apps,
            VisibleToAllUsers=True,
            JobFlowRole='EMR_EC2_DefaultRole',
            ServiceRole='EMR_DefaultRole',
            Tags=tags_param
        )

        Logger.log.info("Cluster ID: %s" % cluster_id)
        Logger.log.info("Waiting for cluster to finish bootstrapping and to be in WAITING state.....")

        while True:
            time.sleep(120)
            description = self.client.describe_cluster(
                ClusterId=cluster_id['JobFlowId']
            )
            Logger.log.info("description of the cluster: %s" % description)
            cluster_state = description['Cluster']['Status']['State']
            Logger.log.info("Cluster State: %s" % cluster_state)
            if cluster_state == 'WAITING':
                break

        return cluster_id['JobFlowId'], cluster_id['ClusterArn'], description['Cluster']['MasterPublicDnsName']

    def run_setup_queries(self, jobflow_id, script_location):

        hive_cmd = "hive-script --run-hive-script --hive-versions 2.3.6 --args -f " + script_location

        hive_step = self.client.add_job_flow_steps(
            JobFlowId=jobflow_id,
            Steps=[
                {
                    'Name': 'Hive setup',
                    'ActionOnFailure': 'CONTINUE',
                    'HadoopJarStep': {
                        'Jar': 'script-runner.jar',
                        'Args': [hive_cmd],
                        'Type': 'HIVE',
                    }
                }
            ]
        )

        Logger.log.info("Step IDs: %s" % hive_step)
        step_ids = hive_step['StepIds']
        Logger.log.info("Waiting for step %s to finish." % step_ids[0])

        while True:
            time.sleep(300)
            description = self.client.describe_step(
                ClusterId=jobflow_id,
                StepId=step_ids[0]
            )
            Logger.log.info("description of the step: %s" % description)
            step_state = description['Step']['Status']['State']
            Logger.log.info("step State: %s" % step_state)
            if step_state == 'COMPLETED':
                return True
            if step_state == 'PENDING':
                continue
            else:
                Logger.log.info("Failure Reason: %s" % description['Step']['Status']['FailureDetails']['Reason'])
                Logger.log.info("Failure Message: %s" % description['Step']['Status']['FailureDetails']['Message'])
                Logger.log.info("Failure LogFile: %s" % description['Step']['Status']['FailureDetails']['LogFile'])
                return False

    def terminate_cluster(self, cluster_id):
        self.client.terminate_job_flows(JobFlowIds=[cluster_id])

    def show_session(self, emr_master_dns):
        query_location = "../workloads/tpcds/presto/setup/"
        query_filename = "show_session.sql"
        query_path = query_location + query_filename
        command = 'java -jar ./presto-cli-0.227-executable.jar --server ' + emr_master_dns \
                  + ':8889 --catalog hive -f' + query_path

        stdout, stderr, return_code = execute_shell_command(command)
        Logger.log.info("Output: %s, Error -> %s, return_code: %s" % (stdout, stderr, return_code))

        if return_code == 0:
            return True
        else:
            return False

    def run_query(self, query_name, query_path, emr_master_dns):
        query_passed = True
        Logger.log.info("Query Name: %s" % query_name)
        command = 'java -jar ./presto-cli-0.227-executable.jar --server ' + emr_master_dns \
                  + ':8889 --catalog hive --schema ' + str(os.getenv('db_name')) + ' --output-format CSV -f ' \
                  + query_path

        stdout, stderr, return_code = execute_shell_command(command)
        Logger.log.info("Output: %s, Error -> %s, return_code: %s" % (stdout, stderr, return_code))
        query_passed, query_info, query_stats, error_info, elapsed_time = self.get_query_info(emr_master_dns)

        return query_passed, query_info, query_stats, elapsed_time, stdout, error_info

    def get_query_info(self, emr_master_dns):
        request_url = "http://" + emr_master_dns + ":8889/v1/query/"

        query_dict_list = requests.get(request_url).json()
        query_info = query_dict_list[0]
        for q in query_dict_list:
            if query_info['queryId'] < q['queryId']:
                query_info = q

        query_response_url = request_url + query_info['queryId']
        query_full_json = requests.get(query_response_url).json()
        elapsed_time = to_seconds(query_full_json['queryStats']['elapsedTime'])

        query_stats = dict()
        query_stats['totalPlanningTime'] = query_full_json['queryStats']['totalPlanningTime']
        query_stats['totalTasks'] = query_full_json['queryStats']['totalTasks']
        query_stats['peakUserMemoryReservation'] = query_full_json['queryStats']['peakUserMemoryReservation']
        query_stats['peakTotalMemoryReservation'] = query_full_json['queryStats']['peakTotalMemoryReservation']
        query_stats['totalCpuTime'] = query_full_json['queryStats']['totalCpuTime']
        query_stats['totalBlockedTime'] = query_full_json['queryStats']['totalBlockedTime']
        query_stats['rawInputDataSize'] = query_full_json['queryStats']['rawInputDataSize']

        query_state = query_full_json['state']

        error_info = dict()
        if query_state == 'FINISHED':
            query_passed = True
        else:
            query_passed = False
            error_info['errorType'] = query_full_json['errorType']
            error_info['errorCode'] = query_full_json['errorCode']
            if 'failureInfo' in query_stats:
                error_info['failureInfo']['type'] = query_full_json['failureInfo']['type']
                error_info['failureInfo']['message'] = query_full_json['failureInfo']['message']

        return query_passed, query_full_json, query_stats, error_info, elapsed_time

