class Builder:

    def create_cluster(self, cluster_name, log_uri, release_label, instance_type, instance_count, ec2_key_name,
                       aws_subnet_id, aws_security_group, applications, tags, configurations):
        pass

    def run_setup_queries(self, jobflow_id, script_location):
        pass

    def terminate_cluster(self, cluster_id):
        pass

    def show_session(self, emr_master_dns):
        pass

    def run_query(self, query, name, emr_master_dns):
        pass
