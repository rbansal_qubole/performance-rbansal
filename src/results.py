import json
from collections import OrderedDict


class Results:

    def __init__(self):
        self.all_query_engine_runtime = dict()
        self.best_query_engine_runtime = dict()
        self.all_query_stats = dict()
        self.best_query_stats = dict()
        self.failed_queries = dict()

    def update_pass_results(self, query_filename, runtime, query_stats):
        runtimes = self.all_query_engine_runtime.get(query_filename, list())
        stats = self.all_query_stats.get(query_filename, list())
        runtimes.append(runtime)
        stats.append(query_stats)
        self.all_query_engine_runtime[query_filename] = runtimes
        self.all_query_stats[query_filename] = stats

        best_runtime = self.best_query_engine_runtime.get(query_filename, runtimes[0])
        if runtime <= best_runtime:
            self.best_query_engine_runtime[query_filename] = runtime
            self.best_query_stats[query_filename] = query_stats

        # dumping the engine runtimes in a file
        f = open("../logs/query_engine_runtimes.json", "w+")
        json.dump(OrderedDict(sorted(self.all_query_engine_runtime.items())), f, indent=4)
        f.close()

        # dumping the query stats in a file
        f = open("../logs/query_stats.json", "w+")
        json.dump(OrderedDict(sorted(self.all_query_stats.items())), f, indent=4)
        f.close()

    def update_output(self, query_filename, stdout):
        # writing output of query in a file
        output_filename = query_filename + "_output.txt"
        f = open("../logs/output_files/" + output_filename, "w+")
        f.write(str(stdout))
        f.close()

    def update_failed_queries(self, filename, error_info):

        self.failed_queries[filename] = error_info

        # dumping failed queries with the error message in a file
        f = open("../logs/failed_queries.json", "w+")
        json.dump(OrderedDict(sorted(self.failed_queries.items())), f, indent=4)
        f.close()

    def dump_results(self):
        # write query_engine_runtimes in a file
        f = open("../logs/best_query_engine_runtime.json", "w+")
        json.dump(OrderedDict(sorted(self.best_query_engine_runtime.items())), f, indent=4)
        f.close()

        # write query_engine_runtimes in a file
        f = open("../logs/best_query_stats.json", "w+")
        json.dump(OrderedDict(sorted(self.best_query_stats.items())), f, indent=4)
        f.close()
