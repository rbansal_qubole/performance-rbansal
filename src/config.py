from builder.presto_emr_builder import PrestoEMRBuilder


class Config:

    def __init__(self, config):
        # cluster config
        # EMR cluster parameters
        self.cluster_name = config.getoption('cluster_name')
        self.release_label = config.getoption('release_label')
        self.instance_type = config.getoption('instance_type')
        self.instance_count = config.getoption('instance_count')
        self.ec2_key_name = config.getoption('ec2_key_name')
        self.aws_subnet_id = config.getoption('aws_subnet_id')
        self.aws_security_group = config.getoption('aws_security_group')
        self.log_uri = config.getoption('log_uri')
        self.applications = config.getoption('applications')
        self.tags = config.getoption('tags')
        self.emr_master_dns = config.getoption('emr_master_dns')

        # test config
        self.workload_loc = config.getoption('workload_loc')
        self.db_name = config.getoption('db_name')
        self.query_iteration = config.getoption('query_iteration')
        #self.list_of_queries_to_run = config.getoption('list_of_queries_to_run')
        self.skip_setup_queries = config.getoption('skip_setup_queries')

        # if platform is EMR, use PrestoEMRBuilder
        self.builder = PrestoEMRBuilder(self.emr_master_dns)



