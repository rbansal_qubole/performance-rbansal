import pytest
import os
from src.results import Results


def pytest_addoption(parser):
    # EMR cluster parameters
    parser.addoption('--create_cluster', default=True, dest='create_cluster',
                     help='Create Cluster before starting the test')
    parser.addoption('--terminate_cluster', default=True, dest='terminate_cluster',
                     help='Terminate the cluster after test finishes')
    parser.addoption('--cluster_name', default='emr_cluster_perf', dest='cluster_name',
                     help='Name of the cluster to be created or already existing cluster name')
    parser.addoption('--release_label', default='emr-5.28.0', dest='release_label',
                     help='Release version to be used to create cluster(use latest), eg. emr-5.28.0')
    parser.addoption('--instance_type', default='m5.xlarge', dest='instance_type',
                     help='Type of master and slave EC2 instances in the cluster, '
                          'for trial:use m5.xlarge, for perf run:use r5d.4xlarge')
    parser.addoption('--instance_count', default=2, dest='instance_count',
                     help='No of EC2 instances (1 master + n slaves)')
    parser.addoption('--ec2_key_name', default='perf-ec2-keypair', dest='ec2_key_name', help='EC2 KeyPair name')
    parser.addoption('--aws_subnet_id', default='subnet-030fed1e831723b4c', dest='aws_subnet_id',
                     help='Public Subnet ID of the VPC in which cluster will be created')
    parser.addoption('--aws_security_group', default='sg-0d1a5c91bb6b695f6', dest='aws_security_group',
                     help='Master and Slave security group in the same VPC')
    parser.addoption('--log_uri', default='s3://rbansal-demo/', dest='log_uri', help='Log URI in S3 for logs')
    parser.addoption('--applications', default='Hive,Presto', dest='applications',
                     help='Comma separated application names to be installed on the cluster')
    parser.addoption('--tags', default='owner=rbansal', dest='tags',
                     help='Comma separated key=value Tags to be assigned to cluster so as to identify')
    config = '[{"Classification":"presto-config","Properties":{"query.max-memory-per-node":"78GB",' \
             '"query.max-total-memory-per-node":"78GB"}}]'
    parser.addoption('--configurations', default=config, dest='configurations',
                     help='json string for customizing applications configurations')
    parser.addoption('--emr_master_dns', default='', dest='emr_master_dns', help='dns of master node in AWS EMR')

    # setup parameters
    parser.addoption('--skip_setup_queries', default='false', dest='skip_setup_queries',
                     help='Skip the setup queries part, if setup query has already been run')
    parser.addoption('--setup_script_location', default=None, dest='setup_script_location',
                     help='S3 location of your Hive script.')

    # test parameters
    parser.addoption('--run_tests', default='true', dest='run_tests',
                     help='set it false, if you want to create cluster only, and no tests')
    parser.addoption('--workload_loc', default='../workloads/tpcds/presto/cross_release/', dest='workload_loc',
                     help='location of workload (test queries set) in the repo')
    parser.addoption('--db_name', default='default', dest='db_name', help='existing db name')
    parser.addoption('--query_iteration', default=1, dest='query_iteration',
                     help='no of iterations for each query')
    parser.addoption('--list_of_queries_to_run', default='all', dest='list_of_queries_to_run',
                     help='Comma separated list of queries to run')


@pytest.fixture(scope="module")
def results():
    results = Results()
    return results


def pytest_configure(config):

    # cluster configs
    os.environ["create_cluster"] = config.getoption('create_cluster')
    os.environ["terminate_cluster"] = config.getoption('terminate_cluster')

    if config.getoption('create_cluster') == 'true':
        os.environ["cluster_name"] = config.getoption('cluster_name')
        os.environ["release_label"] = config.getoption('release_label')
        os.environ["instance_type"] = config.getoption('instance_type')
        os.environ["instance_count"] = str(config.getoption('instance_count'))
        os.environ["ec2_key_name"] = config.getoption('ec2_key_name')
        os.environ["aws_subnet_id"] = config.getoption('aws_subnet_id')
        os.environ["aws_security_group"] = config.getoption('aws_security_group')
        os.environ["log_uri"] = config.getoption('log_uri')
        os.environ["applications"] = config.getoption('applications')
        os.environ["tags"] = config.getoption('tags')
        os.environ["configurations"] = config.getoption('configurations')

    if config.getoption('create_cluster') == 'false':
        os.environ["emr_master_dns"] = config.getoption('emr_master_dns')

    # setup configs
    os.environ["skip_setup_queries"] = config.getoption('skip_setup_queries')
    if config.getoption('skip_setup_queries') == 'false':
        os.environ["setup_script_location"] = config.getoption('setup_script_location')

    # test configs
    os.environ["run_tests"] = config.getoption('run_tests')
    if config.getoption('run_tests') == 'true':
        os.environ["workload_loc"] = config.getoption('workload_loc')
        os.environ["db_name"] = config.getoption('db_name')
        os.environ["query_iteration"] = str(config.getoption('query_iteration'))
        os.environ["list_of_queries_to_run"] = config.getoption('list_of_queries_to_run')


