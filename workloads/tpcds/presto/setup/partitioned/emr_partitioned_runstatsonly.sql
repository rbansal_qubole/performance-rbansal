
USE tpcds_orc_1000_presto_forcol_partitioned;

analyze table call_center compute statistics noscan;
analyze table call_center compute statistics for columns;
analyze table call_center compute statistics;

analyze table catalog_page compute statistics noscan;
analyze table catalog_page compute statistics for columns;
analyze table catalog_page compute statistics;

analyze table catalog_returns partition(cr_returned_date_sk) compute statistics noscan;
analyze table catalog_returns partition(cr_returned_date_sk) compute statistics for columns;
analyze table catalog_returns partition(cr_returned_date_sk) compute statistics;

analyze table catalog_sales partition(cs_sold_date_sk) compute statistics noscan;
analyze table catalog_sales partition(cs_sold_date_sk) compute statistics for columns;
analyze table catalog_sales partition(cs_sold_date_sk) compute statistics;

analyze table customer compute statistics noscan;
analyze table customer compute statistics for columns;
analyze table customer compute statistics;

analyze table customer_address compute statistics noscan;
analyze table customer_address compute statistics for columns;
analyze table customer_address compute statistics;

analyze table customer_demographics compute statistics noscan;
analyze table customer_demographics compute statistics for columns;
analyze table customer_demographics compute statistics;

analyze table date_dim compute statistics noscan;
analyze table date_dim compute statistics for columns;
analyze table date_dim compute statistics;

analyze table household_demographics compute statistics noscan;
analyze table household_demographics compute statistics for columns;
analyze table household_demographics compute statistics;

analyze table income_band compute statistics noscan;
analyze table income_band compute statistics for columns;
analyze table income_band compute statistics;

analyze table inventory compute statistics noscan;
analyze table inventory compute statistics for columns;
analyze table inventory compute statistics;

analyze table item compute statistics noscan;
analyze table item compute statistics for columns;
analyze table item compute statistics;

analyze table promotion compute statistics noscan;
analyze table promotion compute statistics for columns;
analyze table promotion compute statistics;

analyze table reason compute statistics noscan;
analyze table reason compute statistics for columns;
analyze table reason compute statistics;

analyze table ship_mode compute statistics noscan;
analyze table ship_mode compute statistics for columns;
analyze table ship_mode compute statistics;

analyze table store compute statistics noscan;
analyze table store compute statistics for columns;
analyze table store compute statistics;

analyze table store_returns partition(sr_returned_date_sk) compute statistics noscan;
analyze table store_returns partition(sr_returned_date_sk) compute statistics for columns;
analyze table store_returns partition(sr_returned_date_sk) compute statistics;

analyze table store_sales partition(ss_sold_date_sk) compute statistics noscan;
analyze table store_sales partition(ss_sold_date_sk) compute statistics for columns;
analyze table store_sales partition(ss_sold_date_sk) compute statistics;

analyze table time_dim compute statistics noscan;
analyze table time_dim compute statistics for columns;
analyze table time_dim compute statistics;

analyze table warehouse compute statistics noscan;
analyze table warehouse compute statistics for columns;
analyze table warehouse compute statistics;

analyze table web_page compute statistics noscan;
analyze table web_page compute statistics for columns;
analyze table web_page compute statistics;

analyze table web_returns partition(wr_returned_date_sk) compute statistics noscan;
analyze table web_returns partition(wr_returned_date_sk) compute statistics for columns;
analyze table web_returns partition(wr_returned_date_sk) compute statistics;

analyze table web_sales partition(ws_sold_date_sk) compute statistics noscan;
analyze table web_sales partition(ws_sold_date_sk) compute statistics for columns;
analyze table web_sales partition(ws_sold_date_sk) compute statistics;

analyze table web_site compute statistics noscan;
analyze table web_site compute statistics for columns;
analyze table web_site compute statistics;